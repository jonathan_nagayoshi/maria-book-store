import * as React from 'react';
import { Container, Book, Details, Flex } from './styles';
import { Loading } from '../../components/Loading';
import { BookStore } from '../../stores/books';
import { inject, observer } from 'mobx-react';
import { RouteComponentProps } from 'react-router-dom';
import AddToCart from '../../components/AddToCart';

interface Params {
  id: string;
}

export interface BookDetailsProps extends RouteComponentProps<Params> {
}

interface InjectedProps extends BookDetailsProps {
  books: BookStore;
}

@inject("books")
@observer
class BookDetails extends React.Component<BookDetailsProps> {

  get injected() {
    return this.props as InjectedProps;
  }

  componentDidMount() {
    this.loadBookDetails();
  }

  loadBookDetails = () => {
    const { id } = this.injected.match.params;

    this.injected.books.getBookData(id);
  }
  
  render() {
    return this.injected.books.isLoading ? (
        <Container> 
          <Flex>
            <Loading/>
          </Flex>
        </Container>
      ) : (
        this.renderDetails()
    );
  }

  renderDetails = () => {
    const { book } = this.injected.books;
    const price: string = parseFloat(book.price).toFixed(2)
    return (
      <Container>
        <Book>
          <img src={book.thumbnail} alt={book.title}/>
          <Details>
            <h1>{book.title}</h1>
            <small>by {book.author}</small>
            <p>{book.description}</p>
            <AddToCart 
              id={book.id}
              price={+price} 
            />
          </Details>
        </Book>
      </Container>
    )
  }
}

export default BookDetails;