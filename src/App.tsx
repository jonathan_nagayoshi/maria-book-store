import React from 'react';
import { GlobalStyle } from './styles/global';
import { BrowserRouter } from "react-router-dom";
import { Provider } from 'mobx-react';
import Header from './components/Header';
import Routes from './routes';
import Stores from "./stores";

const App = () => (
  <React.Fragment>
    <Provider {...Stores}>
      <BrowserRouter>
        <Header />
        <Routes />
      </BrowserRouter>
    </Provider>
    
    <GlobalStyle />
  </React.Fragment>
)

export default App;
