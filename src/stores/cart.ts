import { observable } from "mobx";
import { API_URL } from "../services";
import { Book } from "./books";

export interface BooksInCart {
  id: number;
  thumbnail: string;
  title: string;
  price: string;
  qtd: number;
  subtotal: string;
}

export interface TotalDetails {
  quantity: number;
  price: string;
  discount: string;
  total: string;
}

export class CartStore {
  @observable subtotal: string = '';
  @observable cart: BooksInCart[] = [];
  @observable showMessage: boolean = false;
  @observable discount: number = 0;
  @observable total: number = 0;
  @observable totalDetails: TotalDetails | undefined;

  public getBookSubtotal(quantity: number, price: number) {
    const subtotal = quantity * price;
    this.subtotal = subtotal.toFixed(2);
  }

  public async addBookToCart(id: number, quantity: number) {
    const targetBook = this.cart.find(item => item.id === id);
    
    if(targetBook) {
      targetBook.qtd = targetBook.qtd + quantity;
      targetBook.subtotal = (Number(targetBook.price) * targetBook.qtd).toString();
      this.showMessage = true;
      setTimeout(() => this.showMessage = false, 2000);
    } else {
      try {
        const response = await fetch(`${API_URL}/books/${id}`);
        const data: Book = await response.json();
        const book: BooksInCart = {
          id: data.id,
          thumbnail: data.thumbnail,
          title: data.title,
          price: data.price,
          qtd: quantity,
          subtotal: this.subtotal
        }
  
        this.cart.push(book);
        this.showMessage = true;
        setTimeout(() => this.showMessage = false, 2000);
      } catch(error) {
        console.log(error)
      }
    }
  }

  public removeBookFromCart(book: BooksInCart) {
    this.cart = this.cart.filter(item => item.id !== book.id);
  }

  public getTotal(quantities: number[], subtotals: number[]) {
    const quantityTotal = quantities.reduce((a, b) => a + b, 0);
    const priceTotal = subtotals.reduce((a, b) => a + b, 0);

    switch(quantityTotal) {
      case 1: 
        this.discount = 0;
        break;
      case 2: 
        this.discount = priceTotal*5/100;
        break;
      case 3: 
        this.discount = priceTotal*10/100;
        break;
      case 4:
        this.discount = priceTotal*20/100;
        break;
      default: 
        this.discount = priceTotal*25/100; 
    }

    this.total = priceTotal - this.discount;

    this.totalDetails = {
      quantity: quantityTotal,
      price: priceTotal.toString(),
      discount: this.discount.toString(),
      total: this.total.toString()
    }
  }
}