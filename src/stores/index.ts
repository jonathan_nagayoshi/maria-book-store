import { BookStore } from './books';
import { CartStore } from './cart';
 
export default {
  books: new BookStore(),
  cart: new CartStore()
};