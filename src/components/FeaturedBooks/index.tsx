import * as React from 'react';
import { Row, Title, Box, About } from './styles';
import { BookStore } from '../../stores/books';
import { inject, observer } from 'mobx-react';
import { Loading } from '../../components/Loading';

interface InjectedProps {
  books: BookStore;
}

const FeaturedBooks = (props: any) => {
  const injected = props as InjectedProps;

  return (
    <React.Fragment>
      <Title>Featured Books {injected.books.isLoading && <Loading/>}</Title>
      <Row>
        {injected.books.list.filter(book => book.featured === true).map(book => (
          <Box 
            to={`/book/${book.id}`} 
            key={book.id} 
            primarycolor={book.primaryColor!} 
            secondarycolor={book.secondaryColor!}>

            <img src={book.thumbnail} alt={book.title} />
            
            <About primarycolor={book.primaryColor!}>
              <strong>{book.title}</strong>
              <small>by {book.author}</small>
              <p>{book.description}</p>
              <span>R$ {parseFloat(book.price).toFixed(2)}</span>
            </About>
          </Box> 
        ))} 
      </Row>
    </React.Fragment>
  )
};

export default inject("books")(observer(FeaturedBooks));