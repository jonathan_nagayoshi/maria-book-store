import * as React from 'react';
import { TotalDetails } from '../../stores/cart';
import { PurchaseDetails } from './styles';

export interface TotalProps {
  totalDetails: TotalDetails;
}

const Total = (props: TotalProps) => (
  <div>
    <PurchaseDetails>
      <li>
        <span>Books Quantity:</span>
        <span>{props.totalDetails.quantity}</span>
      </li>
      <li>
        <span>Books total price:</span>
        <span>R$ {parseFloat(props.totalDetails.price).toFixed(2)}</span>
      </li>
      <li>
        <span>Discount:</span>
        <span> - R$ {parseFloat(props.totalDetails.discount).toFixed(2)}</span>
      </li>
      <li>
        <span>Total:</span>
        <span>R$ {parseFloat(props.totalDetails.total).toFixed(2)}</span>
      </li>
    </PurchaseDetails>
  </div>
);

export default Total;