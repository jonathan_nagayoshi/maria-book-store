import * as React from 'react';
import Icon from '../../assets/images/loading.svg';
import { Spinner } from './styles';

export const Loading = () => <Spinner src={Icon} alt="Carregando" />