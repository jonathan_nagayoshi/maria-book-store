import * as React from 'react';
import { Row, SuccessMessage, ErrorMessage } from './styles';
import { CartStore } from '../../stores/cart';
import { inject, observer } from 'mobx-react';
import { observable } from 'mobx';

export interface AddToCartProps {
  id: number;
  price: number;
}

interface InjectedProps extends AddToCartProps {
  cart: CartStore;
}

@inject("cart")
@observer
class AddToCart extends React.Component<AddToCartProps> {
  @observable quantity: number = 0;
  @observable error: boolean = false;

  get injected() {
    return this.props as InjectedProps;
  }

  getInputValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.quantity = +e.target.value;
    this.error = false;
    this.injected.cart.getBookSubtotal(this.quantity, this.props.price);
  }

  addBook = () => {
    if(this.quantity > 0) {
      this.injected.cart.addBookToCart(this.props.id, this.quantity);
    } else {
      this.error = true;
    }
  }

  render() {
    return (
      <React.Fragment>
        <strong>R$ {this.quantity > 1 ? this.injected.cart.subtotal : this.props.price}</strong>
          <Row>
            <input 
              type="number" 
              placeholder="Quantity"
              onChange={this.getInputValue}
            />
            <button onClick={this.addBook}>Add to cart</button>
            {this.injected.cart.showMessage && <SuccessMessage>Book added to cart</SuccessMessage>}
            {this.error && <ErrorMessage>Please, add a quantity</ErrorMessage>}
          </Row>
      </React.Fragment>
    )
  };
}

export default AddToCart;