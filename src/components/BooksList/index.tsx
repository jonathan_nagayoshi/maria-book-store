import * as React from 'react';
import { Title, Row, Item } from './styles';
import { Loading } from '../../components/Loading';
import { inject, observer } from 'mobx-react';
import { BookStore } from '../../stores/books';

interface InjectedProps {
  books: BookStore;
}

@inject("books")
@observer
class BooksList extends React.Component {

  get injected() {
    return this.props as InjectedProps;
  }

  componentDidMount() {
    this.injected.books.getBooks();
  }

  render() {
    return (
      <React.Fragment>
        <Title>Browse {this.injected.books.isLoading && <Loading/>}</Title>
        <Row>
          {this.injected.books.list.map(book => (
            <Item to={`/book/${book.id}`} key={book.id}>
              <img src={book.thumbnail} alt={book.title}/>
              <div>
                <strong>{book.title}</strong>
                <small>by {book.author}</small>
                <span>R${parseFloat(book.price).toFixed(2)}</span>
              </div>
            </Item>
          ))}
        </Row>
      </React.Fragment>
    )
  }
}

export default BooksList;